# File Transfer Network #

## Compile file ##
terminal write $ make

## Server File Transfer ##
./server <port server>

## Client File Transfer##
./client <ip server> <port server> <directory file>

## Linux List The Open Ports And The Process ##

sudo lsof -i   
sudo netstat -lptu  
sudo netstat -tulpn   