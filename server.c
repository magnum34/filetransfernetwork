#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
//htons
#include <arpa/inet.h>
//uint32_t htonl(uint32_t hostlong);
//uint16_t htons(uint16_t hostshort);
//file
#include  <fcntl.h>

#define BUFFERT 512
#define BACKLOG 1
struct  sockaddr_in sock_serv,sock_client;

int createSocketServer(int port){
	
	/*
	 *int socket(int domain, int type, int protocol); 
	 */
	int socketServer;
	int length;
	int yes = 1;
	//SOCK_STREAM – TCP (komunikacja dwukierunkowa
	//AF_INET - IPv4
	socketServer = socket(AF_INET,SOCK_STREAM,0);
	if(socketServer == -1){
		perror("socket fail");
		return EXIT_FAILURE;
	}
	/*
	 * int setsockopt(int socket, int level, int option_name, const void
                *option_value, socklen_t option_len);
	 */ 
	if(setsockopt(socketServer, SOL_SOCKET, SO_REUSEADDR,&yes,sizeof(int)) == -1 ) {
        perror("setsockopt error");
        exit(5);
    }

    length = sizeof(struct sockaddr_in);
    //zeruje strukturę
    bzero(&sock_serv,length);
    //AF_INET – IPv4 (protokół rodziny TCP/IP)
    sock_serv.sin_family=AF_INET;
    sock_serv.sin_port=htons(port);
    sock_serv.sin_addr.s_addr=htonl(INADDR_ANY);
    
    
	if(bind(socketServer,(struct sockaddr*)&sock_serv,length)==-1){
		perror("bind fail");
		return EXIT_FAILURE;
	}
	return socketServer;
}

int main(int argc, char* argv[] ){
	int sfd,file;
	unsigned int length = sizeof(struct sockaddr_in);
	unsigned int nsid;
	unsigned long int memmory;
	unsigned long int nbytes, count=0;
	ushort client_port;
	char buffer[BUFFERT];
	//INET_ADDRSTRLEN - rozmiar nagłowka IPv4
	char dest[INET_ADDRSTRLEN];
	char filename[256];
	if(argc != 2){
		printf("Error  usage: %s <port servera>\n",argv[0]);
	}
	
	//tworzymy gniazdo
	sfd = createSocketServer(atoi(argv[1]));

    
    bzero(buffer,BUFFERT);
	//int listen(int socket, int backlog);
	listen(sfd, BACKLOG);
	printf("Wating...\n");
    nsid = accept(sfd,(struct sockaddr*)&sock_client,&length);
    if(nsid == -1){
		perror("accept fail");
		return EXIT_FAILURE;	
	}else{
		//inet_ntop - convert IPv4 and IPv6 addresses from binary to text form
		if(inet_ntop(AF_INET,&sock_client.sin_addr,dest,INET_ADDRSTRLEN)==NULL){
			perror("error socket");
			exit(4);
		}
		client_port=ntohs(sock_client.sin_port);
		printf("Connect client : %s:%d\n",dest,client_port);
		
		//name files
		sprintf(filename,"output");
		printf("Creating the copied output file name : %s\n",filename);
		
		if((file=open(filename,O_CREAT|O_WRONLY,0755)) == -1){
			perror(" open file fail");
			exit(3);
		}
		bzero(buffer,BUFFERT);
		//ssize_t recv(int socket, void *buffer, size_t length, int flags);
		nbytes=recv(nsid,buffer,BUFFERT,0);
		while(nbytes){
			if(nbytes==-1){
				perror("Receive error");
				exit(5);
			}
			//zapis
			if((memmory=write(file,buffer,nbytes))==-1){
				perror("Write file error");
				exit(5);
			}
			count=count+memmory;
			bzero(buffer,BUFFERT);
			nbytes=recv(nsid,buffer,BUFFERT,0);
		}
		printf("ilosc danych bez naglowkow: %lld bajty\n",count);
		printf("Odebrane pakiety od klienta %s:%d\n",dest,client_port);
		close(file);
		close(sfd);
		
	}
    close(nsid);
	return 0;
}
