#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
//htons
#include <arpa/inet.h>
//uint32_t htonl(uint32_t hostlong);
//uint16_t htons(uint16_t hostshort);
#include <fcntl.h>
//Typ pliku struktura
#include <sys/stat.h>
//off_t
#include <sys/types.h>


#define BUFFERT 512
#define BACKLOG 1
struct  sockaddr_in sock_serv;

int createClientSocket (int port, char* ipaddr){
    int length;
	int sfd;
    
	sfd = socket(PF_INET,SOCK_STREAM,0);
	if (sfd == -1){
        perror("socket fail");
        return EXIT_FAILURE;
	}
    
	length=sizeof(struct sockaddr_in);
	bzero(&sock_serv,length);
	
	sock_serv.sin_family=AF_INET;
	sock_serv.sin_port=htons(port);
    if (inet_pton(AF_INET,ipaddr,&sock_serv.sin_addr)==0){
		printf("Invalid IP adress\n");
		return EXIT_FAILURE;
	}
    
    return sfd;
}
int main(int argc, char* argv[] ){
	int file;
	int sfd;
	int length = sizeof(struct sockaddr_in);
	char buffor[BUFFERT];
	unsigned long int nbytes;
	//przesunięcie o 64 bity
	off_t count,size,packet;
	struct stat buffer_file;
	if (argc != 4){
		printf("Error usage : %s <ip_serv> <port_serv> <filename>\n",argv[0]);
		return EXIT_FAILURE;
	}
    sfd=createClientSocket(atoi(argv[2]),argv[1]);
    if((file = open(argv[3],O_RDONLY)) == -1){
		perror("open file fail");
		return EXIT_FAILURE;
	}
	if(stat(argv[3],&buffer_file) == -1){
		perror("Buffer file fail");
		return EXIT_FAILURE;
	}else{
		//całkowity rozmiar z nagłówkiem
		size=buffer_file.st_size;
	}
	bzero(&buffor,BUFFERT);
	
	if(connect(sfd,(struct sockaddr*)&sock_serv,length)==-1){
		perror(" Connection Server error\n");
		exit(3);
	}
	//odczyt pliku
	nbytes = read(file,buffor,BUFFERT);
	//wysyłanie pliku
	while(nbytes){
		if(nbytes==-1){
			perror("read fails");
			return EXIT_FAILURE;
		}
		packet =  sendto(sfd,buffor,nbytes,0,(struct sockaddr*)&sock_serv,length);
		if(packet == -1){
			perror("send error");
			return EXIT_FAILURE;
		}
		bzero(&buffor,BUFFERT);
		count+=packet;
		nbytes = read(file,buffor,BUFFERT);
	}
	packet =  sendto(sfd,buffor,0,0,(struct sockaddr*)&sock_serv,length);
	printf("Ilosc danych przeslanych do servera z naglowkami : %lld bajty\n",count);
	printf("Ukonczono wysylanie pliku do servera\n");
	
	close(sfd);
	return 0;
}

